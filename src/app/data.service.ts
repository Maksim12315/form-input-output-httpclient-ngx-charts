import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private url = 'http://localhost:3000/csv';

  constructor(private httpClient: HttpClient) { }

  
  // for receive data for pie chart 
  getValueForPie(startDate, endDate, param): Observable<any> {
    let amount = [];
    return this.httpClient.get(this.url)
    .pipe(map(data=>{
      for(let i in data) {
          if (data[i].week_ref >= startDate &&
            data[i].week_ref <= endDate) {
              amount.push({
                "name": data[i].item_category,
                "value": (param === 'markdown') ? data[i].markdown : (param === 'revenues') ? data[i].revenues : data[i].margin
              })
        }
      }
      return this.squeezeArr(amount);
    }));
  }

  // for receive data for line chart 
  getValueForLine(startDate, endDate, param, categories): Observable<any> {
    let amount = [];
    return this.httpClient.get(this.url)
    .pipe(map(data=>{
      for(let i in data) {
          if (data[i].week_ref >= startDate &&
            data[i].week_ref <= endDate) {
              for(let j = 0; j < categories.length; j++) {
                if(data[i].item_category === categories[j]) {
                  amount.push({   
                    "value" : (param === 'markdown') ? data[i].markdown : (param === 'revenues') ? data[i].revenues : data[i].margin,
                    "name" : data[i].week_ref
                  });
                }
              }
        }
      }
      return this.squeezeArr(amount);
    }));
  }

  //support function for basic get-data functions
  squeezeArr(arr) {
    let sqz = [];
    sqz.push(arr[0]);
    for(let i = 1; i < arr.length; i++) {
      for(let j = 0; j < sqz.length; j++) {
        if(arr[i]['name'] === sqz[j]['name']) {
          sqz[j]['value'] += arr[i]['value'];
			      break;
        } else { 
			      if(j == (sqz.length - 1)){
          		sqz.push(arr[i]);
        	  }
        }
      }
    }
    return sqz;
  }

  //function for getting weeks to insert them into select or select is replaced with an input
  getWeeks(): Observable<any> {
    let weeks = new Set();
    return this.httpClient.get(this.url)
    .pipe(map(data=>{
      for(let i in data) {
          weeks.add(data[i].week_ref);
      }
      return weeks;
    }));
  }
}
