import { Component, OnInit } from '@angular/core';
import { DataService } from './data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  
  pieChartData: {name: string, value: number};
  lineChartData: [{name: string, series: [{value: number, name: number}]}]

  categories: string[] = ["QQ", "GG", "FF", "AA", "CC", "SS", "HH", "LL", "DD", "MM", "PP", "NN", "BB", "EE"];
  startDate: number = 27;
  endDate: number = 38;
  param: string = 'markdown';
 

  constructor(private dataService: DataService) {
  }

  ngOnInit() {
    this.redrawGraph();
  }

  //when tracked parameters change
  onChanged(settings){
    this.startDate = settings.start;
    this.endDate = settings.end;
    this.param = settings.parameter;
    this.redrawGraph();
  }

  // categories ang date necessarily receive from server
  resetAll() {
    this.categories = ["QQ", "GG", "FF", "AA", "CC", "SS", "HH", "LL", "DD", "MM", "PP", "NN", "BB", "EE"];
    this.startDate = 27;
    this.endDate = 38;
    this.redrawGraph();
  }

  // when selected pie on pie chart
  onSelected(category) {
    this.categories = [category];
    this.getDataForLine();
  }

  async getDataForPie() {
    await this.dataService.getValueForPie(this.startDate, this.endDate, this.param).subscribe( data => {
      this.pieChartData = data;
    });
  }

  async getDataForLine() {
    await this.dataService.getValueForLine(this.startDate, this.endDate, this.param, this.categories).subscribe(data => {
      this.lineChartData = [{name: this.param, series: data}];
    })
  }

  redrawGraph() {
    this.getDataForPie();
    this.getDataForLine();
  }
}
