import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent{
  @Input() start: number;
  @Input() end: number;
  @Output() onChanged = new EventEmitter<{start:number, end: number, parameter: string}>();

  weeks = [27,28,29,30,31,32,33,34,35,36,37,38];
  params = ["markdown", "revenues", "margin"];
  parameter: string = this.params[0];

  checkValue(start, end, parameter) {
    this.onChanged.emit({start, end, parameter});
  }

}
