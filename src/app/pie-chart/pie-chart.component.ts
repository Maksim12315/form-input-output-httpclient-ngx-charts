import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.css']
})
export class PieChartComponent {
  @Input() data;
  @Output() onSelected = new EventEmitter<any>();

  onSelect(event) {
    this.onSelected.emit(event.name);
    console.log(event.name);
  }
}
